package Task

import (
	_ "database/sql"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"

)

type TaskManager struct {
	db *gorm.DB
}

func NewTaskManager() (TaskManagerI, error) {
	tm := TaskManager{}
	var err error
	conStr := "user=postgres dbname=tasks password=123 host=localhost sslmode=disable"
	tm.db, err = gorm.Open("postgres", conStr)
	if err != nil {
		return nil,err
	}
	tm.db.AutoMigrate(&Task{})
	return &tm, nil
}

func (t *TaskManager) Add(ts Task) error {
	if result := t.db.Create(&ts); result.Error != nil {
		return result.Error
	}
	return nil
}

func (t *TaskManager) Assign(id int, assignee string) error {
	t.db.LogMode(true)
	if result := t.db.Model(&Task{}).Where("id = ?", id).Update("Assignee", "Rustam"); result.Error != nil{
		return result.Error
	}
	return nil
}

func (t *TaskManager) MakeDone(id int) error {
	if result := t.db.Model(&Task{}).Where("id = ?", id).Update("Done", true); result.Error != nil{
		return result.Error
	}
	return nil
}

func (t *TaskManager) ListUnfinished() ([]Task, error){
	var tasks []Task
	if result := t.db.Where("done = ?", false).Find(&Task{}); result.Error != nil{
		return tasks, nil
	}
	return tasks, nil
}

func (t *TaskManager) GetAll() ([]Task, error) {
	var tasks []Task
	if result := t.db.Find(&tasks); result.Error != nil {
		return tasks, nil
	}
	return tasks, nil
}