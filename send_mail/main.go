package main

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
	"net/smtp"

	"time"
)

const (

	// name of the service
	name        = "mailservice"
	description = "My mail Service"

	//port which daemon should be listen
	port = ":9977"
)
type MailManager struct {
	db *gorm.DB
}

type Mail struct{
	gorm.Model
	Mail	string 	`db:"not null "`
	Text	string	`db:"not null"`
	Status	bool	`db:"not null"`
}

type MailServiceI interface {
	Add (ct Mail) error
	Update (i uint, ml Mail) error
	Del (i int) error
	GetAll () ([]Mail, error)
	CheckStatus() ([]*Mail, error)
}


func NewMailManager() (MailServiceI, error) {
	mm := MailManager{}
	var err error
	connStr := "user=postgres dbname=sendmail password=123 host=localhost sslmode=disable"
	mm.db, err = gorm.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}
	mm.db.AutoMigrate(&Mail{})
	return &mm, err
}


func (m *MailManager) Add(ct Mail) error {
	m.db.LogMode(true)
	if result := m.db.Create(&ct); result.Error != nil{
		return result.Error
	}
	return nil
}

func (m *MailManager) Update(i uint, ml Mail) error {

	if result := m.db.Model(&ml).Where("id = ?", i).Updates(&ml); result.Error !=nil{
		return result.Error
	}
	return nil
}

func (m *MailManager) Del(i int) error {
	if result := m.db.Where("id = ?", i).Delete(&Mail{}); result.Error != nil{
		return result.Error
	}
	return nil
}

func (m *MailManager) GetAll() ([]Mail, error) {
	var Mails []Mail
	result := m.db.Find(&Mails)
	if result.Error !=nil{
		return nil, result.Error
	}
	return Mails, nil
}


func (m *MailManager) CheckStatus () ([]*Mail, error) {
	var mails []*Mail
	m.db.LogMode(true)
	result := m.db.Where("status = ?", false).Find(&mails)
	if result.Error != nil {
		return nil, result.Error
	}
	return mails, result.Error
}


func send(body string, mail string ) error{
	from := "kadirovmamur99@gmail.com"
	pass := "1234@jack"
	to := mail

	msg := "From: " + from + "\n" +
		"To: " + to + "\n" +
		"Subject: Hello there\n\n" +
		body

	err := smtp.SendMail("smtp.gmail.com:587",
		smtp.PlainAuth("", from, pass, "smtp.gmail.com"),
		from, []string{to}, []byte(msg))

	if err != nil {
		return err
	}

	log.Print("Sent")
	return nil
}

func main() {
	mm, err := NewMailManager()
	if err != nil {
		log.Fatal("Error while connecting to DB", err)
	}
	c := make(chan string)

	is_shutting_down := false
	started := false


	OUTTER:
	for {
		if is_shutting_down{
			break
		}
		select {
		case s, ok := <-c:

			if ok {
				fmt.Println("\n\nservice is going down...\n\n")
				fmt.Println(fmt.Sprint("\n\nReceived signal %x\n\n",s))
				is_shutting_down = true
				c = nil
				continue OUTTER
			}
			default:
				var UnckeckedMails, err = mm.CheckStatus()
				if err != nil {
					log.Fatal("error")
				}
				for _, element := range UnckeckedMails{
					err := send("test", element.Mail)
					if err != nil {
						log.Fatal("error while sending mail")
					}
					element.Status = true
					err = mm.Update(element.ID, *element)
					if err != nil {
						log.Fatal("s")
					}
				}


				if !started{
					fmt.Println("\n\nSuccessfully\n\n")
					started = true
				}
				t := time.Now()
				fmt.Println(fmt.Sprint("Service is running...\n", t.Format(time.RFC3339)))
				time.Sleep(15000 * time.Millisecond)
		}

	}
	fmt.Println("\n\nThis service has finally shutted down\n\n");
}