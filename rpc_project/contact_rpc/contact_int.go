package main

import (
	"github.com/jinzhu/gorm"
)

type Contact struct {
	gorm.Model
	Name string
	Age int
	Gender string
	Number string
}

type ContactManagerI interface {
	Add (ct Contact, reply *Contact) error
	Update (ct Contact, reply *Contact) error
	Del (i int, reply *Contact) error
	GetAll (ct Contact, reply *[]Contact) error
}
