package main



import "fmt"



func main(){
  

var n uint64
  
fmt.Scan(&n)

  
if n==1 || n==2{
    
fmt.Println(1)
    
  return
  
}

  
var fib []uint64 = make([]uint64, n)
  
fib[0] = 1
  
fib[1] = 1
  
var i uint64 = 2  
for ; i < n; i++{
    
fib[i]=fib[i-1]+fib[i-2]
  
}
  
  
fmt.Println(fib[n-1])

}
