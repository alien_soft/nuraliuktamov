package Animal

import "github.com/jinzhu/gorm"

type Animal struct {
	gorm.Model
	Name string
	Species string
	Age int
	Gender bool
}

type AnimalManagerI interface {
	Add (an Animal) error
	Update (ID int, an Animal) error
	Del (i int) error
	GetAll () ([]Animal, error)
	ListAll () error
}

