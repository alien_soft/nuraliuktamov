package contacts

import ("testing"
_"fmt"
"github.com/jmoiron/sqlx"
)

var (
	cm ContactManagerI
	db *sqlx.DB
)

func TestContactManager(t *testing.T) {
	var err error
	cm, err = NewContactManager()
	if(err != nil) {
		t.Error("Can't add to database", err)
	}
}

func TestContactManager_Add(t *testing.T) {
	err := cm.Add(Contact{"Rustam Turgunov", 23, "male", "998998082596"})

		if (err != nil) {
			t.Error("Contact not added")
	}
}

func TestContactManager_UpdateNumber(t *testing.T) {
	err := cm.UpdateNumber(0,"+998998812891")
		if (err != nil) {
			t.Error("Number not updated")
	}
}

func TestContactManager_GetAll(t *testing.T) {
		_, err := cm.GetAll()
		if (err != nil) {
			t.Error("Cannot get all contacts: ", err)
		}
}

func TestContactManager_ListAll(t *testing.T) {
	err := cm.ListAll()
	if (err != nil) {
		t.Error("Can't list all contacts", err)
	}
}

func TestContactManager_Del(t *testing.T) {
	err := cm.Del(0)
	if (err != nil) {
		t.Error("Contact not deleted", err)
	}
}