package main

import (
	"github.com/jinzhu/gorm"
)

type Animal struct {
	gorm.Model
	Name string
	Species string
	Age int
	Gender bool
}

type AnimalManagerI interface {
	Add (an Animal, reply *Animal) error
	Update (an Animal, reply *Animal) error
	Del (an Animal, reply *Animal) error
	GetAll (an Animal, reply *[]Animal) error
}