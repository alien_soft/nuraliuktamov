package main

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"log"
	"net/rpc"
)

type Task struct{
	gorm.Model
	Name string
	Assignee string
	Time string
	Done bool
}

func main() {
	var reply Task

	var Tasks []Task

	client, err := rpc.DialHTTP("tcp", "localhost:4040")
	if err != nil {
		log.Fatal("Connection error: ", err)
	}

	item := Task{Name:"Fibonacci", Assignee:"Rustam", Time:"05.02.2018", Done:false}

	err = client.Call("TaskManager.Add", item, &reply)
	if err != nil{
		log.Fatal("Error adding Task: ", err)
	}

	err = client.Call("TaskManager.Assign", Task{Model: gorm.Model{ID: 4}, Name:"Odd even",  Assignee:"Rustam", Time:"10.10.2018", Done:false}, &Task{})
	if err != nil{
		log.Fatal("Error while assign Task: ", err)
	}

	err = client.Call("TaskManager.MakeDone", 5, &Task{})
	if err != nil {
		log.Fatal("Error while do make done: ", err)
	}

	err = client.Call("TaskManager.ListUnfinished", Task{}, &Tasks)
	if err != nil {
		log.Fatal("Error while list unfinished tasks: ", err)
	}
	fmt.Println(Tasks)
}
