package animals


import (
	"github.com/jmoiron/sqlx"
	"testing"
)

var (
	am AnimalManagerI
	db *sqlx.DB
)


func TestAnimalManger(t *testing.T) {
	var err error
	am, err = NewAnimalManager()
	if(err != nil){
		t.Error("Can't connect to database")
	}
}

func TestAnimalManagerAdd(t *testing.T){
	err := am.Add(Animal{Name:"Bob", Species:"Dog", Age:3, Gender:true})
	if (err != nil) {
		t.Error("Animal not added", err)
	}
}


func TestAnimalMangerUpdate(t *testing.T) {

	err := am.Update(1, Animal{Name: "Put", Species:"Cat", Age:3, Gender:true})

	if (err != nil) {
		t.Error("Animal not updated")
	}

}

func TestContactMangerGetAll(t *testing.T) {
	_, err := am.GetAll()
	if (err != nil) {
		t.Error("Cannot get all animals: ", err)
	}
}

func TestContactMangerDel(t *testing.T) {
	err := am.Del(2)
	if (err != nil) {
		t.Error("Animal not deleted", err)
	}
}
