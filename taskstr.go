package main

import "fmt"

 type Task struct {
	name string
	assignee string
	title string
	time string
	done bool
}

type TaskManager struct {
	tasks []Task
}

func NewTaskManager() *TaskManager{
	tm := TaskManager{}
	tm.tasks = []Task{}
	return &tm
}

func (t Task) print(){
	fmt.Println(t.name)
	fmt.Println(t.assignee)
	fmt.Println(t.title)
	fmt.Println(t.time)
	fmt.Println(t.done)
}

func (t *TaskManager) add(ta Task){
	t.tasks = append(t.tasks, ta)
}

func (t *TaskManager) assign(i int, as string){
	t.tasks[i].assignee = as
}

func (t *TaskManager) del(i int){
	t.tasks = append(t.tasks[:i], t.tasks[i+1:]...)
}

func (t *TaskManager) makeDone(i int){
	t.tasks[i].done = true
}

func (t *TaskManager) printUnfinished(){
	for _, value := range t.tasks{
		if value.done == false{
			value.print()
		}
	}
}

func (t *TaskManager) printAll(){
	for _, value := range t.tasks{
		value.print()
	}
}

func main(){
	tm := NewTaskManager()
	fmt.Println("Add a new task")
	tm.add(Task{"Fibonachchi", "Nurali", "Solve the fibonachchi from 1 to 100", "23.09.2019", true})
	tm.add(Task{"structure", "Nodir", "Structure of Contacts", "25.09.19", true})
	tm.assign(0, "Rustam")
	tm.tasks[0].print()
	tm.del(0)
	tm.printAll()
	tm.printUnfinished()
}



