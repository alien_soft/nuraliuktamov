package main
import "fmt"
 
func main() {
    add(1, 2, 3)        // sum = 6
    add(1, 2, 3, 4)     // sum = 10
    add(5, 6, 7, 2, 3)  // sum = 23
}
 
func add(numbers ...int){
    var sum = 0
    for _, number := range numbers{
        sum += number
    }
    fmt.Println("sum = ", sum)
}
