package Task

import (
	"github.com/jmoiron/sqlx"
	"testing"
)
var (
	tm TaskManagerI
	db *sqlx.DB
)
func TestTaskManager(t *testing.T) {
	var err error
	tm, err = NewTaskManager()
	if err != nil {
		t.Error("Can't connect to database")
	}

}

func TestTaskManager_Add(t *testing.T) {
	err := tm.Add(Task{Name:"Fibonacci", Assignee:"Nurali", Time:"05.10.2019", Done:true})
	if err != nil {
		t.Error("Task not added", err)
	}
}

func TestTaskManager_Assign(t *testing.T) {
	err := tm.Assign(6,"Rustam")
	if err != nil {
		t.Error("Assignee not added")
	}
}

func TestNewTaskManager_Del (t *testing.T) {
	err := tm.MakeDone(0)
	if err != nil {
		t.Error("Can't do make done", err)
	}
}

func TestTaskManager_ListUnfinished(t *testing.T) {
	_, err := tm.ListUnfinished()
	if (err != nil){
		t.Error("Can't list unfinished tasks", err)
	}
}

func TestTaskManager_GetAll(t *testing.T) {
	_, err := tm.GetAll()
	if (err != nil) {
		t.Error("Can't list all tasks", err)
	}
}

