package main

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"log"
	"net/rpc"
)

type Contact struct{
	gorm.Model
	Name	string 	`db:"not null"`
	Age		int		`db:"not null"`
	Gender	string	`db:"not null"`
	Number	string	`db:"not null"`
}

func main() {
	var reply Contact

	var contacts []Contact

	client, err := rpc.DialHTTP("tcp", "localhost:4045")
	if err != nil {
		log.Fatal("Connection error: ", err)
	}

	item := Contact{Name:"Rollton", Age:4, Gender:"male", Number:"998909958879"}

	err = client.Call("ContactManager.Add", item, &reply)
	if err != nil{
		log.Fatal("Error adding contact: ", err)
	}

	err = client.Call("ContactManager.Update", Contact{Model: gorm.Model{ID: 4}, Name:"Washington",  Age:27, Gender:"male", Number:"8852239"}, &Contact{})
	if err != nil{
		log.Fatal("Error while updating contact: ", err)
	}

	err = client.Call("ContactManager.Del", 3, &Contact{})
	if err != nil {
		log.Fatal("Error while deleting: ", err)
	}

	err = client.Call("ContactManager.GetAll", Contact{}, &contacts)
	if err != nil {
		log.Fatal("Error getting all contacts: ", err)
	}
	fmt.Println(contacts)

}


