package main

import (
"github.com/jinzhu/gorm"
)
type Task struct {
	gorm.Model
	Name string
	Assignee string
	Time string
	Done bool
}

type TaskManagerI interface {
	Add (ts Task, reply *Task) error
	Assign (ts Task, reply *Task) error
	MakeDone (id int, reply *Task) error
	ListUnfinished (ts Task, reply *[]Task) error
	GetAll (ts Task, reply *[]Task) error
}
