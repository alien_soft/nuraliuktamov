package models


import (
	"testing"
	_"fmt"
	"github.com/jmoiron/sqlx"
)

var (
	am AnimalManagerI
	db *sqlx.DB
)

func TestAnimalManager(t *testing.T) {
	var err error
	am, err = NewAnimalManager()
	if(err != nil) {
		t.Error("Can not connect to database", err)
	}
}

func  TestAnimalManagerAdd(t *testing.T){
	err := am.Add(Animal{"Kitty", "cat", 3, false})

	if (err != nil) {
		t.Error("Animal not added")
	}
}

func TestAnimalManagerUpdate(t *testing.T) {
	err := am.Update(1, Animal{"Kitty", "dog", 3, false})
		if (err != nil){
			t.Error("Animal not updated")
		}
}

func TestAnimalManagerGetAll(t *testing.T) {
	_, err := am.GetAll()
	if (err != nil) {
		t.Error("Can not get all animals:", err)
	}
}

func TestAnimalManagerDel(t *testing.T) {
	err := am.Del(2)
	if (err != nil) {
		t.Error("Animal not deleted")
	}
}

func TestAnimalManagerListAll(t *testing.T) {
	err := am.ListAll()
	if (err != nil) {
		t.Error("Can not list all animals", err)
	}
}
