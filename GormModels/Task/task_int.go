package Task

import (
	"github.com/jinzhu/gorm"
)
type Task struct {
	gorm.Model
	Name string
	Assignee string
	Time string
	Done bool
}

type TaskManagerI interface {
	Add (ts Task) error
	Assign (id int, assignee string) error
	MakeDone (id int) error
	ListUnfinished () ([]Task, error)
	GetAll () ([]Task, error)
}