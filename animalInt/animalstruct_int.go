package animals

type Animal struct {
	Name string
	Species string
	Age int
	Gender bool
}

type AnimalManagerI interface {
	Add (an Animal) error
	Update (i int, an Animal) error
	Del (i int) error
	GetAll () ([]Animal, error)
	ListAll () error
}
