package Animal

import (
	_ "database/sql"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type AnimalManager struct {
	db *gorm.DB
}


func NewAnimalManager() (AnimalManagerI, error) {
	am := AnimalManager{}
	var err error
	connStr := "user=postgres dbname=animals password=123 host=localhost sslmode=disable"
	am.db, err = gorm.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}
	am.db.AutoMigrate(&Animal{})
	return &am, nil
}

//Adding new Animal to database
func (a *AnimalManager) Add(ct Animal) error {
	//a.db.LogMode(true)
	if result := a.db.Create(&ct); result.Error != nil{
		return result.Error
	}
	return nil
}

//Updating particular Animal
func (a *AnimalManager) Update(i int, ct Animal) error {

	if result := a.db.Model(&ct).Where("id = ?", i).Updates(&ct); result.Error !=nil{
		return result.Error
	}
	return nil
}

//Функция удаления энимала по айди
func (a *AnimalManager) Del(i int) error {
	if result := a.db.Where("id = ?", i).Delete(&Animal{}); result.Error != nil{
		return result.Error
	}
	return nil
}

//Функция распечатки всех контактов
func (a *AnimalManager) GetAll() ([]Animal, error) {
	var Animals []Animal
	result := a.db.Find(&Animals)
	if result.Error !=nil{
		return nil, result.Error
	}
	return Animals, nil
}

func (a *AnimalManager)ListAll() error{
	var Animals []Animal
	result := a.db.Find(&Animals)
	if result.Error !=nil{
		return nil
	}
	fmt.Println(Animals)
	return nil
}
