package animals

import (
	_ "database/sql"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type ContactManager struct {
	db *gorm.DB
}

func NewContactManager() (AnimalManagerI, error) {
	cm := AnimalManager{}
	var err error
	connStr := "user=postgres dbname=contacts password=123 host=localhost sslmode=disable"
	cm.db, err = gorm.Open("postgres", connStr)
	cm.db.AutoMigrate(&Animal{})
	if err != nil {
		return nil, err
	}
	return &am, nil
}

func (c *ContactManager) Add(ct Contact) error {
	if result := c.db.Create(&ct); result.Error != nil {
		return result.Error
	}
	return nil
}

