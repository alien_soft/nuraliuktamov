package animal

import ( 
	
	"github.com/stretchr/testify/assert"
	"testing"
)

var am *AnimalManager

func TestAnimalManagerAdd(t *testing.T) {
	am = NewAnimalManager()
	am.add(Animal{"Brown", "brown", 5, false})
	assert.NotEmpty(t, am.animals, "Animals not added")
}

func TestAnimalManagerUpdate(t* testing.T) {
	am.update(0, Animal{"Lena", "tiger", 3, false})
	assert.Equal(t, "Lena", am.animals[0].name, "Not updated")
}


