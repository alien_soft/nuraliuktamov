package contacts

type Contact struct {
	Name string
	Age int
	Gender string
	Number string
}
type ContactManagerI interface {
	Add (ct Contact) error
	UpdateNumber(i int, Number string) error
	GetAll() ([]Contact, error)
	ListAll () error
	Del (i int) error
}
