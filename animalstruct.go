package animal

import "fmt"

type Animal struct {
	name string
	species string
	age int 
	gender bool
}

type AnimalManager struct {
	animals []Animal
}

func NewAnimalManager() *AnimalManager {
	am := AnimalManager{}
	am.animals = []Animal{}
	return &am
}

func (a Animal) list(){
	fmt.Println(a.name)
	fmt.Println(a.species)
	fmt.Println(a.age)
	fmt.Println(a.gender)
}

func (a *AnimalManager) add(an Animal) {
	a.animals = append(a.animals, an)
}

func (a *AnimalManager) del(i int) {
	a.animals = append(a.animals[:i], a.animals[i+1:]...)
}

func (a *AnimalManager) update(i int, an Animal) {
	a.animals[i] = an
}

func (a *AnimalManager) listAll() {
	for _, value := range a.animals{
		value.list()
	}
}

func (a *AnimalManager) listFemale() {
	for _, value := range a.animals{
		if value.gender == false{
			value.list()
		}
	}
}

func (a *AnimalManager) updateAge(i int, an int) {
	a.animals[i].age = an
}


func main(){
	am := NewAnimalManager()
	am.add(Animal{"Chukky", "Dog", 3, true})
	am.add(Animal{"Linda", "Dog", 2, false})
	am.add(Animal{"Catty", "Cat", 1, false})
	am.add(Animal{"Pitty", "Cat", 3, true})
	am.del(0)
	am.update(0, Animal{"Chris", "Dog", 4, false})
	am.listAll()
	am.listFemale()
}

