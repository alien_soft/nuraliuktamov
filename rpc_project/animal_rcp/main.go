package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
	"net"
	"net/http"
	"net/rpc"
)

func NewAnimalManager() (AnimalManagerI, error) {
	am := AnimalManager{}
	var err error
	conStr := "user=postgres dbname=animals password=123 host=localhost sslmode=disable"
	am.db, err = gorm.Open("postgres", conStr)
	am.db.AutoMigrate(&Animal{})
	if err != nil {
		return nil, err
	}
	return &am, nil
}

type AnimalManager struct {
	db *gorm.DB
}

func (a AnimalManager) Add(an Animal, reply *Animal) error {
	a.db.LogMode(true)
	if result := a.db.Create(&an); result.Error != nil {
		return result.Error
	}
	return nil
}

func (a AnimalManager) Update(an Animal, reply *Animal) error {
	if result := a.db.Model(&Animal{}).Where("id = ?", an.ID).Update("Name", "Chukky"); result.Error != nil{
		return result.Error
	}
	return nil
}

func (a AnimalManager) Del(an Animal, reply *Animal) error {
	if result := a.db.Where("name = ?", an.Name); result.Error != nil {
		return result.Error
	}
	return nil
}

func (a AnimalManager) GetAll(an Animal, reply *[]Animal) error {
	a.db.LogMode(true)
	var animals []Animal
	if result := a.db.Find(&animals); result.Error != nil {
		return result.Error
	}
	*reply = animals
	return nil
}

func main (){
	tm, err := NewAnimalManager()
	if err != nil {
		log.Fatal("Can't connect to database", err)

	}
	err = rpc.Register(tm)
	if err != nil {
		log.Fatal("Couldn't register the api: ", err)
	}

	rpc.HandleHTTP()

	listener, err := net.Listen("tcp", ":4040")

	if err != nil {
		log.Fatal("Can't create listener:", err)
	}

	log.Printf("serving rpc on port %d", 4040)

	err = http.Serve(listener, nil)
	if err != nil {
		log.Fatal("error serving:", err)
	}
}