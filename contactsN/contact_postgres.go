package contacts

import (
	"fmt"
	_ "fmt"
	_ "database/sql"

	_ "github.com/lib/pq"
	"github.com/jmoiron/sqlx"

)

type ContactManager struct {
	db *sqlx.DB
}

func NewContactManager() (ContactManagerI, error) {
	cm := ContactManager{}
	var err error
	cm.db, err = sqlx.Connect("postgres", "user=postgres password=123 dbname=contacts sslmode=disable")
	if err != nil{
		return nil, err
	}
	return &cm, nil
}

func (cm *ContactManager) Add(ct Contact) error {
	insertNew := `Insert into contact(name, age, gender, number) values($1, $2, $3, $4)`
	cm.db.MustExec(insertNew, ct.Name, ct.Age, ct.Gender, ct.Number)
	return nil
}

func (cm *ContactManager) UpdateNumber(i int, Number string) error {
	UpNumber := `update contact set number=$1 where id=$2`
	cm.db.MustExec(UpNumber, Number, i)
	return nil
}

func (c *ContactManager) GetAll() ([]Contact, error) {
	var contacts []Contact
	err := c.db.Select(&contacts, "SELECT name, age, gender, number FROM contact")
	if(err != nil){
		return nil, err
	}
	return contacts, nil
}

func (cm *ContactManager) ListAll() error {
	var contacts []Contact
	err := cm.db.Select(&contacts, "select name, age, gender, number from contact")
	if (err != nil) {
		return err
	}
	fmt.Println(contacts)
	return nil
}

func (cm *ContactManager) Del(i int) error {
	DelCont := `delete from contact where id=$1`
	cm.db.MustExec(DelCont, i)
	return nil
}