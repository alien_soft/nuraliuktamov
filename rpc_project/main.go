package main

import (
	_ "database/sql"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
	"net"
	"net/http"
	"net/rpc"
)

type TaskManager struct {
	db *gorm.DB
}

func NewTaskManager() (TaskManagerI, error) {
	cm := TaskManager{}
	var err error
	connStr := "user=postgres dbname=tasks password=123 host=localhost sslmode=disable"
	cm.db, err = gorm.Open("postgres", connStr)
	cm.db.AutoMigrate(&Task{})
	if err != nil {
		return nil, err
	}
	return &cm, nil
}
func (t *TaskManager) Add(ts Task, reply *Task) error {
	t.db.LogMode(true)
	if result := t.db.Create(&ts); result.Error != nil {
		return result.Error
	}
	return nil
}

func (t *TaskManager) Assign(ts Task, reply *Task) error {
	if result := t.db.Model(&Task{}).Where("id = ?", ts.ID).Update("Assignee", "Rustam"); result.Error != nil{
		return result.Error
	}
	return nil
}

func (t *TaskManager) MakeDone(id int, reply *Task) error {
	if result := t.db.Model(&Task{}).Where("id = ?", id).Update("Done", true); result.Error != nil{
		return result.Error
	}
	return nil
}

func (t *TaskManager) ListUnfinished(ts Task, reply *[]Task) error {
	var tasks []Task
	if result := t.db.Where("done = ?", false).Find(&tasks); result.Error != nil{
		return nil
	}
	*reply = tasks
	return nil
}

func (t *TaskManager) GetAll(ts Task, reply *[]Task) error {
	var tasks []Task
	if result := t.db.Find(&tasks); result.Error != nil {
		return nil
	}
	return nil
}

func main (){
	tm, err := NewTaskManager()
	if err != nil {
		log.Fatal("Can't connect to database", err)
		
	}
	err = rpc.Register(tm)
	if err != nil {
		log.Fatal("Couldn't register the api: ", err)
	}

	rpc.HandleHTTP()

	listener, err := net.Listen("tcp", ":4040")

	if err != nil {
		log.Fatal("Can't create listener:", err)
	}

	log.Printf("serving rpc on port %d", 4040)

	err = http.Serve(listener, nil)
	if err != nil {
		log.Fatal("error serving:", err)
	}
}