package main

import (
	"fmt"
	_ "fmt"
	"github.com/jinzhu/gorm"
	"log"
	"net/rpc"
)

type Animal struct{
	gorm.Model
	Name string
	Species string
	Age int
	Gender bool
}

func main() {
	var reply Animal

	var animals = []Animal{}

	client, err := rpc.DialHTTP("tcp", "localhost:4040")
	if err != nil{
		log.Fatal("Connection error: ", err)
	}

	item := Animal{Name:"Bob", Species:"Dog", Age:3, Gender:true}

	err = client.Call("AnimalManager.Add", item, &reply)
	if err != nil{
	log.Fatal("Error while adding Animal: ", err)
	}
	err = client.Call("AnimalManager.Update", Animal{Model: gorm.Model{ID: 5}, Name:"Chukky", Species:"Dog", Age: 3, Gender: false}, &Animal{})
	if err != nil {
		log.Fatal("Error while update Animals: ", err)
	}

	err = client.Call("AnimalManager.Del", item, &Animal{})
	if err != nil {
		log.Fatal("Error while deleting animal: ", err)
	}

	err = client.Call("AnimalManager.GetAll", Animal{}, &animals)
	if err != nil {
		log.Fatal("Error while getting all animals", err)
	}
	fmt.Println(animals)
	}