package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
	"net"
	"net/http"
	"net/rpc"
)

type ContactManager struct {
	db *gorm.DB
}

func NewContactManager() (ContactManagerI, error) {
	cm := ContactManager{}
	var err error
	connstr := "user=postgres dbname=contacts password=123 host=localhost sslmode=disable"
	cm.db, err = gorm.Open("postgres", connstr)
	cm.db.AutoMigrate(&Contact{})
	if err != nil {
		return nil, err
	}
	return &cm, nil
}

func (c *ContactManager) Add(ct Contact, reply *Contact) error {
	if result := c.db.Create(&ct); result.Error != nil {
		return result.Error
	}
	return nil
}

func (c *ContactManager) Update(ct Contact, reply *Contact) error {
	c.db.LogMode(true)
	if result := c.db.Model(&ct).Where("id = ?", ct.ID).Updates(&ct); result.Error != nil {
		return result.Error
	}
	return nil
}



func (c *ContactManager) Del(i int, reply *Contact) error {
	if result := c.db.Where("id = ?", i).Delete(&Contact{}); result.Error != nil{
		return result.Error
	}
	return nil
}
func (c *ContactManager) GetAll(ct Contact, reply *[]Contact) error {
	c.db.LogMode(true)
	var contacts []Contact
	result := c.db.Find(&contacts)
	if result.Error != nil {
		return result.Error
	}
	*reply = contacts
	return nil
}

func main(){
	cm, err := NewContactManager()
	if err != nil {
		log.Fatal("Coudn't connect to database: ", err)

	}
	err = rpc.Register(cm)
	if err != nil {
		log.Fatal("Couldn't register the api: ", err)
	}

	rpc.HandleHTTP()

	listener, err := net.Listen("tcp", ":4045")

	if err != nil {
		log.Fatal("Can't create listener:", err)
	}

	log.Printf("serving rpc on port %d", 4045)

	err = http.Serve(listener, nil)
	if err != nil {
		log.Fatal("error serving:", err)
	}

}