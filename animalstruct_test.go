package animal
import (
	
	"testing"
)

var am *AnimalManager

func TestAnimalManagerAdd(t *testing.T){
	am = NewAnimalManager()
	am.add(Animal{"Lena", "tiger", 2, true})
	if len(am.animals)<1{
		t.Error("Not added new animal")
	}
}

func TestUpdate(t *testing.T){
	am.update(0, Animal{"Brown", "bird", 5, false})
	if am.animals[0].name == "Lena"{
		t.Error("Not updated")
	}
}

func TestUpdateAge(t *testing.T){
	am.updateAge(0, 7)
	if am.animals[0].age != 7{
		t.Error("Not added new age")
	}
}

func TestDel(t *testing.T){
	am.del(0)
	if len(am.animals) != 0{
		t.Error("Not deleted")
	}
}
